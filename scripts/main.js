// 1.Це об'єктна модель документа (Document Object Model). Браузер створює DOM для того,
// щоб за допомогою JavaScript можна було швидко маніпулювати веб - документом: шукати потрібний елемент,
// додавати нові елементи, отримати наступний дочірний елемент і т.п.

//2.innerText — показує весь текстовий вміст, який не відноситься до синтаксису HTML. Тобто будь-який текст,
// укладений між елементами, буде записаний в innerText. Причому якщо всередині innerText будуть
// ще якісь елементи HTML зі своїм вмістом, він проігнорує самі елементи і поверне їх внутрішній текст.
// innerHTML - Покаже текстову інформацію по одному елементу. Тобто потрапить і текст, і розмітка
// HTML - документа, яка може бути укладена між тегами основного елемента, що відкривають і закривають.

// 3.getElementById(), getElementsByName (), getElementsByTagName (), getElementsByClassName (), querySelectorAll (), querySelector ()
// Найкращі: getElementById(), querySelector(), querySelectorAll () - використовуються найчастіше і вважаються більш зручними

//1
let paragraph = document.getElementsByTagName("p");
[...paragraph].forEach((item) => (item.style.backgroundColor = "#ff0000"));
console.log(paragraph);

//2
const element = document.querySelector("#optionsList");
console.log(element);
console.log(element.parentNode);
console.log(element.childNodes);
element.childNodes.forEach((node) => console.log(node.nodeType));

//3
const testParagraph = document.getElementById("testParagraph");
testParagraph.innerText = "This is a paragraph";

//4
const mainHeader = document.querySelectorAll(".main-header li");
console.log(mainHeader);
Array.from(mainHeader).forEach((item) => {
  item.className = "nav-item";
});

//5
const sectionTitle = document.querySelectorAll(".section-title");
sectionTitle.forEach((item) => item.classList.remove("section-title")); //classList.add/remove/toggle("class") – додати/видалити /перевірає , якщо є - видаляє, нема-додає клас
